"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Weapon_1 = require("./Weapon");
class Hero {
    constructor(nameValue, powerValue, lifeValue) {
        this.name = nameValue;
        this.power = powerValue;
        this.life = lifeValue;
        this.weapon = Weapon_1.Weapon;
    }
    isAlive() {
        return this.life > 0;
    }
    attack(opponent) {
        opponent.life -= this.power;
    }
}
exports.default = Hero;
const joan = new Hero("Joan", 20, 40);
const leon = new Hero("Leon", 40, 50);
joan.attack(leon);
console.log(joan);
console.log(leon);
console.log(joan.isAlive());
// //La méthode `attack` a un paramètre `opponent` (de type `Hero`). Il faut réduire le nombre (`life`) de `opponent` d'autant de dégats (`power`) de l'attaquant.
// Exemple : Si Joan attaque Leon, cela sera représenté par :
// joan.attack(leon)
// 
// La méthode `isAlive` devrait retourner `true` si le nombre de points de vie du héros est supérieur à zéro et `false` sinon.
// ​
// Crée deux instances de `Hero` et vérifie que les méthodes `attack` et `isAlive` fonctionnent.​
