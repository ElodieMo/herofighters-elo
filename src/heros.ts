import {Weapon} from "./Weapon"
export default class Hero {
    

    name: string;
    power: number;
    life: number;
    weapon:Weapon;

    constructor(nameValue: string, powerValue: number, lifeValue: number) {
        this.name = nameValue;
        this.power = powerValue;
        this.life = lifeValue;
        this.weapon=Weapon;

    }


    isAlive(): boolean {
        return this.life > 0;
    }

    attack(opponent:Hero){
        opponent.life-= this.power
            
        }
    }



const joan = new Hero("Joan", 20, 40);
const leon = new Hero("Leon", 40, 50);


joan.attack(leon)

console.log(joan);
console.log(leon);


console.log(joan.isAlive());




// //La méthode `attack` a un paramètre `opponent` (de type `Hero`). Il faut réduire le nombre (`life`) de `opponent` d'autant de dégats (`power`) de l'attaquant.
// Exemple : Si Joan attaque Leon, cela sera représenté par :

// joan.attack(leon)
// 


// La méthode `isAlive` devrait retourner `true` si le nombre de points de vie du héros est supérieur à zéro et `false` sinon.

// ​

// Crée deux instances de `Hero` et vérifie que les méthodes `attack` et `isAlive` fonctionnent.​


